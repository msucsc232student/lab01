# CSC232 - Data Structures with C++

## Lab 1 - Introduction to C++ Classes

### High-level Procedure

* Fork this repo into your own private repo
* Clone your fork
* Checkout the `develop` branch
* Carry out the tasks outlined in your lab handout, committing your work after each task is completed.
* Create a pull request to merge your `develop` branch into your `master` branch.
* Add your instructor as a reviewer to pull request.
* Submit a text submission assignment on Blackboard.

### Building and Testing your code

To build your code, create and navigate to the `build` folder and run `cmake` to generate a `Makefile` to be used by `make`. For example, noting that the `$` is the command line prompt (and therefore **not** to be typed):

```bash
$ mkdir build && cd build
$ cmake -G "Unix Makefiles" ..
... environment specific output...
... over several lines ...
$ make clean && make
Scanning dependencies of target Lab01_Test
[ 25%] Building CXX object CMakeFiles/Lab01_Test.dir/src/test/UnitTestRunner.cpp.o
[ 50%] Linking CXX executable Lab01_Test
[ 50%] Built target Lab01_Test
Scanning dependencies of target Lab01
[ 75%] Building CXX object CMakeFiles/Lab01.dir/src/main/Main.cpp.o
[100%] Linking CXX executable Lab01
[100%] Built target Lab01
$
```

Your output will most likely differ; the important aspect is to note that two executable files were generated:

* `Lab01_Test` - This program executes the unit tests for this lab.
* `Lab01` - This is your playground where you can experiment _using_ the class(es) created in this lab.

