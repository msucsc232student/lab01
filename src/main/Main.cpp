/**
 * CSC232 Data Structures with C++
 * Missouri State University, Fall 2018.
 *
 * @file    Main.cpp
 * @authors Jim Daehn <jdaehn@missouristate.edu>
 *          <FILL ME IN ACCORDINGLY>
 * @brief   Entry point to this application.
 *
 * @copyright Jim Daehn, 2017. All rights reserved.
 */

#include <cstdlib>
#include <iostream>

int main(int argc, char** argv) {
  std::cout << "Welcome to Lab 1" << std::endl;

  return EXIT_SUCCESS;
}
